/*
 * display.c
 *
 *  Created on: Jun 24, 2020
 *      Author: szymon
 */

#include "DSP28x_Project.h"     // Device header file and examples include file

#define N_SEGMENTS   4

static const long int symbols[12] = {
    0b11000000, // 0
    0b11111001, // 1
    0b10100100, // 2
    0b10110000, // 3
    0b10011001, // 4
    0b10010010, // 5
    0b10000010, // 6
    0b11111000, // 7
    0b10000000, // 8
    0b10010000, // 9
    0b10001110, // F
    0b10001100, // P
};

enum disp_state {Idle,Frequency,PhShift,Deadtime};
static int* p_frequency;
static int* p_phase;
static int* p_deadtime;
static long int display_str[4];

struct display
{
    enum disp_state quantity;
    int value;
};

static struct display disp;

void update_display_string(void)
{
    int val = disp.value;
    if (val<0) val = -val;

    int n100 = val/100;
    int n10 = (val%100)/10;
    int n1 = val%10;

    switch(disp.quantity) {
       case Frequency:
           display_str[0] = 0b10001110; // F
           display_str[1] = symbols[n100];
           display_str[2] = symbols[n10];
           display_str[3] = symbols[n1];
           break;
       case PhShift:
           display_str[0] = 0b10001100; // P
           display_str[1] = symbols[n100];
           display_str[2] = symbols[n10];
           display_str[3] = symbols[n1];
           break;
       case Deadtime:
           display_str[0] = 0b10100001; // d
           display_str[1] = symbols[n100];
           display_str[2] = symbols[n10];
           display_str[3] = symbols[n1];
           break;
       default:
           display_str[0] = 0b11111001; // i
           display_str[1] = 0b10100001; // d
           display_str[2] = 0b11000111; // l
           display_str[3] = 0b10000110; // e
    }
}

void set_display_idle(void)
{
    disp.quantity = Idle;
    disp.value = 0;
    update_display_string();
}

void set_display_frequency(void)
{
    disp.quantity = Frequency;
    disp.value = *p_frequency;
    update_display_string();
}

void set_display_ph_shift(void)
{
    disp.quantity = PhShift;
    disp.value = *p_phase;
    update_display_string();
}

void set_display_deadtime(void)
{
    disp.quantity = Deadtime;
    disp.value = *p_deadtime;
    update_display_string();
}

void init_display(int* p_f,int* p_ph,int* p_dt)
{
    p_frequency = p_f;
    p_phase = p_ph;
    p_deadtime = p_dt;
    set_display_idle();
}

// update_display() is run wiith every interrupt of Timer0. At each invocation
// a different 7 segment display is activated.
void update_display(void)
{
    static int segment_number = 0;

    //GpioDataRegs.GPACLEAR.all = 0x00FF0000; // clear bits 16-23
    //GpioDataRegs.GPASET.all = symbols[iSymb]<<16; // set symbol on bits 16-23

    // set display segments
    GpioDataRegs.GPACLEAR.all = 0x00FF0000; // clear bits 16-23
    //GpioDataRegs.GPASET.all = display_str[segment_number]<<16; // set symbol on bits 16-23
    GpioDataRegs.GPASET.all = display_str[segment_number]<<16;

    // set display segments
    long int segment_bits = 0;
    switch (segment_number) {
           case 0: segment_bits = 0x0E000000; break;
           case 1: segment_bits = 0x0D000000; break;
           case 2: segment_bits = 0x0B000000; break;
           case 3: segment_bits = 0x07000000; break;
           default: segment_bits = 0x00000000;
        }
    GpioDataRegs.GPACLEAR.all = 0x0F000000; // clear bits 24-27
    GpioDataRegs.GPASET.all = segment_bits; // set segment on bits 24-27

    segment_number = (segment_number == 0) ? (N_SEGMENTS-1) : (segment_number-1); // cycle to next segment
}
