
void set_display_idle(void);
void set_display_frequency(void);
void set_display_ph_shift(void);
void set_display_deadtime(void);
void update_display_string(void);
void update_display(void);
void init_display(int*,int*,int*);
