/*
 * pwm.h
 *
 *  Created on: Jun 24, 2020
 *      Author: szymon
 */

#ifndef PWM_H_
#define PWM_H_

#endif /* PWM_H_ */

void InitEPwmCommon(unsigned long);
void SetFrequency(int,int);
void SetPhase(int);
void SetDeadtime(int);
void DisablePWM(void);
void EnablePWM(void);
