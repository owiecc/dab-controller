
// Included Files
#include "DSP28x_Project.h"

void InitGpio(void)
{
    EALLOW;
    GpioCtrlRegs.GPAMUX1.all = 0x00005555; // (15-8) GPIOx, (7-0) ePWMx
    GpioCtrlRegs.GPAMUX2.all = 0x00000000; // (31-24) GPIOx, (23-16) GPIOx
    GpioCtrlRegs.GPAPUD.all = 0x000FF00;  // (31-0) pull-up
    GpioCtrlRegs.GPADIR.all = 0xFFFF00FF;  // (31-24) 7-segment cathodes, (23-16) 7-segment anodes, (15-8) switches, (7-0) PWM
    EDIS;
}

void SetDeadtime(int dt)
{
    // Fcpu = 150MHz
    // dt = 0xF -> deadtime = 200ns
    // dt = 0x1 -> deadtime = 13.333ns

    int deadtime = (dt*3)/40;

    EPwm1Regs.DBRED = deadtime;
    EPwm1Regs.DBFED = deadtime;
    EPwm2Regs.DBRED = deadtime;
    EPwm2Regs.DBFED = deadtime;
    EPwm3Regs.DBRED = deadtime;
    EPwm3Regs.DBFED = deadtime;
    EPwm4Regs.DBRED = deadtime;
    EPwm4Regs.DBFED = deadtime;
}

void SetPhase(int ph)
{
    // ph = [-500, 500] -> Φ = [-π/2, π/2]
    int prd = EPwm1Regs.TBPRD;
    long int half_prd = prd/2;
    long int prd_shift = half_prd*ph/1000;

    EPwm1Regs.TBPHS.half.TBPHS = 0;
    EPwm2Regs.TBPHS.half.TBPHS = half_prd;
    EPwm3Regs.TBPHS.half.TBPHS = half_prd - prd_shift;;
    EPwm4Regs.TBPHS.half.TBPHS = (prd_shift == 0) ? 0 : prd - prd_shift;
}

void SetFrequency(int f, int ph)
{
    // 110kHz*680*2 = 150MHz
    // f*prd*2 = 150MHz
    // prd = 75000/f[kHz]
    int prd = 75000/f;
    int half_prd = prd/2;

    EPwm1Regs.TBPRD = prd;
    EPwm2Regs.TBPRD = prd;
    EPwm3Regs.TBPRD = prd;
    EPwm4Regs.TBPRD = prd;

    EPwm1Regs.CMPA.half.CMPA = half_prd;
    EPwm2Regs.CMPA.half.CMPA = half_prd;
    EPwm3Regs.CMPA.half.CMPA = half_prd;
    EPwm4Regs.CMPA.half.CMPA = half_prd;

    SetPhase(ph);
}

void InitEPwmCommon(unsigned long deadband)
{
    SetFrequency(100, 0);                            // 100kHz, 0 phase shift

    EPwm1Regs.TBCTR = 0;                            // Clear counter
    EPwm2Regs.TBCTR = 0;
    EPwm3Regs.TBCTR = 0;
    EPwm4Regs.TBCTR = 0;

    EPwm1Regs.TBCTL.bit.CTRMODE = TB_COUNT_UP;
    EPwm2Regs.TBCTL.bit.CTRMODE = TB_COUNT_UP;
    EPwm3Regs.TBCTL.bit.CTRMODE = TB_COUNT_UP;
    EPwm4Regs.TBCTL.bit.CTRMODE = TB_COUNT_UP;

    EPwm1Regs.TBCTL.bit.PHSEN = TB_DISABLE;
    EPwm2Regs.TBCTL.bit.PHSEN = TB_ENABLE;
    EPwm3Regs.TBCTL.bit.PHSEN = TB_ENABLE;
    EPwm4Regs.TBCTL.bit.PHSEN = TB_ENABLE;

    EPwm1Regs.TBCTL.bit.SYNCOSEL = TB_CTR_ZERO;     // Master unit
    EPwm2Regs.TBCTL.bit.SYNCOSEL = TB_SYNC_IN;
    EPwm3Regs.TBCTL.bit.SYNCOSEL = TB_SYNC_IN;
    EPwm4Regs.TBCTL.bit.SYNCOSEL = TB_SYNC_IN;

    EPwm1Regs.CMPCTL.bit.SHDWAMODE = CC_SHADOW;     // Load registers every ZERO
    EPwm1Regs.CMPCTL.bit.SHDWBMODE = CC_SHADOW;
    EPwm1Regs.CMPCTL.bit.LOADAMODE = CC_CTR_ZERO;
    EPwm1Regs.CMPCTL.bit.LOADBMODE = CC_CTR_ZERO;

    // Set actions
    EPwm1Regs.AQCTLA.bit.ZRO = AQ_SET;
    EPwm1Regs.AQCTLA.bit.CAU = AQ_CLEAR;
    EPwm2Regs.AQCTLA.bit.ZRO = AQ_SET;
    EPwm2Regs.AQCTLA.bit.CAU = AQ_CLEAR;
    EPwm3Regs.AQCTLA.bit.ZRO = AQ_SET;
    EPwm3Regs.AQCTLA.bit.CAU = AQ_CLEAR;
    EPwm4Regs.AQCTLA.bit.ZRO = AQ_SET;
    EPwm4Regs.AQCTLA.bit.CAU = AQ_CLEAR;

    // Active HI complementary PWMs + setup deadband
    EPwm1Regs.DBCTL.bit.OUT_MODE = DB_FULL_ENABLE;
    EPwm1Regs.DBCTL.bit.POLSEL = DB_ACTV_HIC;
    EPwm1Regs.DBCTL.bit.IN_MODE = DBA_ALL;
    EPwm1Regs.DBRED = deadband;
    EPwm1Regs.DBFED = deadband;

    EPwm2Regs.DBCTL.bit.OUT_MODE = DB_FULL_ENABLE;
    EPwm2Regs.DBCTL.bit.POLSEL = DB_ACTV_HIC;
    EPwm2Regs.DBCTL.bit.IN_MODE = DBA_ALL;
    EPwm2Regs.DBRED = deadband;
    EPwm2Regs.DBFED = deadband;

    EPwm3Regs.DBCTL.bit.OUT_MODE = DB_FULL_ENABLE;
    EPwm3Regs.DBCTL.bit.POLSEL = DB_ACTV_HIC;
    EPwm3Regs.DBCTL.bit.IN_MODE = DBA_ALL;
    EPwm3Regs.DBRED = deadband;
    EPwm3Regs.DBFED = deadband;

    EPwm4Regs.DBCTL.bit.OUT_MODE = DB_FULL_ENABLE;
    EPwm4Regs.DBCTL.bit.POLSEL = DB_ACTV_HIC;
    EPwm4Regs.DBCTL.bit.IN_MODE = DBA_ALL;
    EPwm4Regs.DBRED = deadband;
    EPwm4Regs.DBFED = deadband;

    // Trip action set to force-low for output A
    EALLOW;
    EPwm1Regs.TZCTL.bit.TZA = 0b10;
    EPwm2Regs.TZCTL.bit.TZA = 0b10;
    EPwm3Regs.TZCTL.bit.TZA = 0b10;
    EPwm4Regs.TZCTL.bit.TZA = 0b10;

    EPwm1Regs.TZCTL.bit.TZB = 0b10;
    EPwm2Regs.TZCTL.bit.TZB = 0b10;
    EPwm3Regs.TZCTL.bit.TZB = 0b10;
    EPwm4Regs.TZCTL.bit.TZB = 0b10;

    EPwm1Regs.TZSEL.bit.OSHT1 = 1;
    EPwm2Regs.TZSEL.bit.OSHT1 = 1;
    EPwm3Regs.TZSEL.bit.OSHT1 = 1;
    EPwm4Regs.TZSEL.bit.OSHT1 = 1;
    EDIS;

    // Interrupt where we will change the Deadband
    EPwm1Regs.ETSEL.bit.INTSEL = ET_CTR_ZERO;     // Select INT on Zero event
    EPwm1Regs.ETSEL.bit.INTEN = 1;                // Enable INT
    EPwm1Regs.ETPS.bit.INTPRD = ET_3RD;           // Generate INT on 3rd event
}

void DisablePWM(void) {
    EALLOW;
    EPwm1Regs.TZFRC.bit.OST = 0x1;
    EPwm2Regs.TZFRC.bit.OST = 0x1;
    EPwm3Regs.TZFRC.bit.OST = 0x1;
    EPwm4Regs.TZFRC.bit.OST = 0x1;
    EDIS;
}

void EnablePWM(void) {
    EALLOW;
    EPwm1Regs.TZCLR.bit.OST = 0x1;
    EPwm2Regs.TZCLR.bit.OST = 0x1;
    EPwm3Regs.TZCLR.bit.OST = 0x1;
    EPwm4Regs.TZCLR.bit.OST = 0x1;
    EDIS;
}
