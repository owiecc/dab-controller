// Included Files
#include "DSP28x_Project.h"     // Device header file and examples include file
#include "pwm.h"
#include "display.h"
#include "input.h"

// Function Prototypes
__interrupt void cpu_timer0_isr(void);
__interrupt void epwm1_isr(void);

// Globals
Uint32  PhaseShift;

// Defines
#define EPWM_DB   0x000F

#define FMIN   20
#define FNOM   100
#define FMAX   200
#define PHMIN   0
#define PHNOM   0
#define PHMAX   500
#define DTMIN   40
#define DTNOM   100
#define DTMAX   500

enum converter_states {Idle,RunFreqCtrl,RunPhaseCtrl,RunDeadtimeCtrl,Reset};
static enum converter_states converter_state;

enum buttons {On=0, LL=2, L=1, Zero=3, R=4, RR=5, Off=6};

void main(void)
{
    // Initialize System Control: PLL, WatchDog, enable Peripheral Clocks
    InitSysCtrl();

    InitGpio();

    // Clear all interrupts and initialize PIE vector table: Disable CPU interrupts
    DINT;
    InitPieCtrl();
    IER = 0x0000;
    IFR = 0x0000;
    InitPieVectTable();
    EALLOW;
    PieVectTable.EPWM1_INT = &epwm1_isr;
    PieVectTable.TINT0 = &cpu_timer0_isr;
    EDIS;

    // Initialize all the device peripherals:
    EALLOW;
    SysCtrlRegs.PCLKCR0.bit.TBCLKSYNC = 0;
    EDIS;

    InitEPwmCommon(EPWM_DB);
    InitCpuTimers();
    ConfigCpuTimer(&CpuTimer0, 150, 5000); // 5ms
    CpuTimer0Regs.TCR.all = 0x4000; // TSS bit = 0

    EALLOW;
    SysCtrlRegs.PCLKCR0.bit.TBCLKSYNC = 1;
    EDIS;

    // User specific code, enable interrupts
    
    // Enable CPU INT3 which is connected to EPWM1-3 INT
    IER |= M_INT3;
    // Enable CPU INT1 which is connected to CPU-Timer 0
    IER |= M_INT1;

    // Enable EPWM INTn in the PIE: Group 3 interrupt 1-3
    PieCtrlRegs.PIEIER3.bit.INTx1 = 1;
    // Enable TINT0 in the PIE: Group 1 interrupt 7
    PieCtrlRegs.PIEIER1.bit.INTx7 = 1;

    // Enable global Interrupts and higher priority real-time debug events
    EINT;       // Enable Global interrupt INTM
    ERTM;       // Enable Global realtime interrupt DBGM

    // Init variables
    static int frequency = FNOM;
    static int phase = PHNOM;
    static int deadtime = DTNOM;

    converter_state = Idle;

    init_display(&frequency,&phase,&deadtime);

    for(;;)
    {
        int btn = button_pressed();

        switch (converter_state)
        {
            case Idle:
                {
                    DisablePWM();
                    set_display_idle();

                    if (btn == On) converter_state = RunFreqCtrl;
                    if (btn == Off) converter_state = Reset;
                    break;
                }
            case Reset:
                {
                    DisablePWM();
                    set_display_idle();
                    // reset all variable to nominal values
                    frequency = FNOM;
                    phase = PHNOM;
                    deadtime = DTNOM;

                    converter_state = Idle;
                    break;
                }
            case RunFreqCtrl:
                {
                    EnablePWM();
                    set_display_frequency();

                    if (btn == LL) frequency-=10;
                    if (btn == L) frequency--;
                    if (btn == Zero) frequency=FNOM;
                    if (btn == R) frequency++;
                    if (btn == RR) frequency+=10;

                    frequency = (frequency < FMIN) ? FMIN : frequency;
                    frequency = (frequency > FMAX) ? FMAX : frequency;

                    if ((btn == LL)|(btn == L)|(btn == Zero)|(btn == R)|(btn == RR)) SetFrequency(frequency,phase);

                    if (btn == Off) converter_state = Idle;
                    if (btn == On) converter_state = RunPhaseCtrl;
                    break;
                }
            case RunPhaseCtrl:
                {
                    EnablePWM();
                    set_display_ph_shift();

                    if (btn == LL) phase-=10;
                    if (btn == L) phase--;
                    if (btn == Zero) phase=PHNOM;
                    if (btn == R) phase++;
                    if (btn == RR) phase+=10;

                    phase = (phase < PHMIN) ? PHMIN : phase;
                    phase = (phase > PHMAX) ? PHMAX : phase;

                    if ((btn == LL)|(btn == L)|(btn == Zero)|(btn == R)|(btn == RR)) SetPhase(phase);

                    if (btn == Off) converter_state = Idle;
                    if (btn == On) converter_state = RunDeadtimeCtrl;
                    break;
                }
            case RunDeadtimeCtrl:
                {
                    EnablePWM();
                    set_display_deadtime();

                    if (btn == LL) deadtime-=10;
                    if (btn == L) deadtime--;
                    if (btn == Zero) deadtime=DTNOM;
                    if (btn == R) deadtime++;
                    if (btn == RR) deadtime+=10;

                    deadtime = (deadtime < DTMIN) ? DTMIN : deadtime;
                    deadtime = (deadtime > DTMAX) ? DTMAX : deadtime;

                    if ((btn == LL)|(btn == L)|(btn == Zero)|(btn == R)|(btn == RR)) SetDeadtime(deadtime);

                    if (btn == Off) converter_state = Idle;
                    if (btn == On) converter_state = RunFreqCtrl;
                    break;
                }
            default: __asm("          NOP");
        }
    }
}

__interrupt void cpu_timer0_isr(void)
{
    update_display();
    PieCtrlRegs.PIEACK.all = PIEACK_GROUP1;
}

__interrupt void epwm1_isr(void)
{
    // Clear INT flag for this timer
    EPwm1Regs.ETCLR.bit.INT = 1;

    // Acknowledge this interrupt to receive more interrupts from group 3
    PieCtrlRegs.PIEACK.all = PIEACK_GROUP3;
}


